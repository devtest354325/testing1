﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ApiTesting.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet("test1")]
        public IEnumerable<WeatherForecast> test1()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpGet("test2")]
        public object test2()
        {
            return new { a = "hello", b = "world" };
        }
        [HttpGet("test3")]
        public object test3()
        {
            var lst = new List<object>();
            lst.Add(new { name = "dara", sex = "m" });
            lst.Add(new { name = "vanny", sex = "f" });
            return lst;
        }
        [HttpGet("test4")]
        public object test4()
        {

            return new { test = "hello world" };
        }

        [HttpGet("test5")]
        public object test5()
        {

            return new { test = "This is test5" };
        }
        [HttpGet("test6")]
        public object test6()
        {

            return new { test = "This is test6" };
        }

        [HttpGet("test7")]
        public object test7()
        {

            return new { test = "This is test7" };
        }

        [HttpGet("test8")]
        public object test8()
        {
            return new { test = "This is test8" };
        }

        [HttpGet("test9")]
        public object test9()
        {
            return new { test = "This is test9" };
        }


    }
}
