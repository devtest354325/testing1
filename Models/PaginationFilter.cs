﻿using System;

namespace DataAccess.Models.Filter
{
    public class PaginationFilter
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public long TotalRecords { get; set; }
        public string Search { get; set; }
       

        public PaginationFilter()
        {
            this.PageNumber = 1;
            this.PageSize = 1000000;
        }

        public PaginationFilter(int pageNumber, int pageSize, string Search)
        {
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize > 0 ? pageSize : 20;
            this.Search = Search;
        }

    }
   
}
